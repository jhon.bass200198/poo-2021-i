package Pregunta2;

public class Directorio {
    private String Nombre;
    private String Apellidos;
    private String Telefono;
    private int edad;

    Directorio (String Nombre, String Apellidos, String Telefono, int edad){
        this.edad = edad;
        this.Nombre = Nombre;
        this.Apellidos = Apellidos;
        this.Telefono = Telefono;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public String getApellidos(){
        return Apellidos;
    }
    public void setApellidos(String Apellidos){
        this.Apellidos = Apellidos;
    }
    public String getTelefono(){
        return Telefono;
    }
    public void setTelefono(String Telefono){
        this.Telefono = Telefono;
    }
}
