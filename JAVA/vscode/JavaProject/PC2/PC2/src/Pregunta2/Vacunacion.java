package Pregunta2;
import java.util.HashSet;
import java.util.Set;

public class Vacunacion {
    public static void main(String...args){
        Directorio p1 = new Directorio("Miguel","Gonzales","987678765",20);
        Directorio p2 = new Directorio("Miguel","Gonzales","987678765",20);
        Directorio p3 = new Directorio("Jose","Gonzales","985654254",23);
        Directorio p4 = new Directorio("Roberto","Perez","985654432",26);
        
        Set<Directorio> paciente = new HashSet(); 
        paciente.add(p1);
        paciente.add(p2);
        paciente.add(p3);
        paciente.add(p4);
        for(Directorio s : paciente) {
            System.out.println(s.getNombre()+" "+s.getApellidos()+" "+s.getTelefono()+" "+s.getEdad());
        }
    
    }
}
