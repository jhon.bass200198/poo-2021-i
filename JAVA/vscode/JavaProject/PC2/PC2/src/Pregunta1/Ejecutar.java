package Pregunta1;

public class Ejecutar {
    public static void main(String[] args){
        Dinero moneda = new Dinero();
        moneda.setYen(10);
        moneda.setCambioDolar(0.092);
        moneda.setCambioSol(3.75);
        Object valorDolar = moneda.getYen() * moneda.getCambioDolar();
        Object valorSol = moneda.getCambioDolar()*moneda.getCambioSol();
        moneda.Imprimir();
        System.out.println("El valor de "+moneda.getYen()+" yenes en dolares es: "+ valorDolar);
        System.out.println("El valor de "+ valorDolar + " en soles es: " + valorSol);
        
    }
}
