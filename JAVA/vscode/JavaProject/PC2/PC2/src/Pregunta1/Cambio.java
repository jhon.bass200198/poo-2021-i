package Pregunta1;

public abstract class Cambio {
    private double yen;
    private double CambioDolar;
    private double CambioSol;
    
    public double getYen() {
        return yen;
    }

    public void setYen(double yen) {
        this.yen = yen;
    }

    public double getCambioDolar() {
        return CambioDolar;
    }

    public void setCambioDolar(double cambioDolar) {
        CambioDolar = cambioDolar;
    }

    public double getCambioSol() {
        return CambioSol;
    }

    public void setCambioSol(double cambioSol) {
        CambioSol = cambioSol;
    }

    public Cambio() {
        this.yen = yen;
        this.CambioDolar = CambioDolar;
        this.CambioSol = CambioSol;
    }
    abstract public void Imprimir();
}
