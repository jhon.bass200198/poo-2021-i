package ParcialP1;

import java.util.HashSet;
import java.util.Set;

public class Armado {
    
    public static void main(String...args){
        Libreria L1 = new Libreria("100 años de soledad","Marquez","Crisol",1990,"COSD234F");
        Libreria L2 = new Libreria("Paco Yunque","Llosa","Peruanisima",1990,"ASKF343F");
        Libreria L3 = new Libreria("Historia del Perú","Varios","PeruNuevo",2000,"AFED109Y");
        Libreria L4 = new Libreria ("El mundo es Ancho y Ajeno","Alegria","Norteñito",2001,"JIK987R");
        
        Set<Libreria> libro = new HashSet(); 
        libro.add(L1);
        libro.add(L2);
        libro.add(L3);
        libro.add(L4);
        
        for(Libreria s : libro) {
           System.out.println(s.getNombre()+" - "+s.getAutor()+" - "+s.getEditorial()+" - "+s.getFecha()+" - "+s.getCodigo());
        }
        System.out.println("Esta historia se basa en ...");
        //Falto imprimir con indice 
    }
}
