package ParcialP3;

public abstract class Venta {
    private int cantidad;
    private String marca;
    private double precio;

    public int getCantidad() {
        return cantidad;
    }
    public Venta() {
        this.cantidad = cantidad;
        this.marca = marca;
        this.precio = precio;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    abstract public void Imprimir();
    
}
