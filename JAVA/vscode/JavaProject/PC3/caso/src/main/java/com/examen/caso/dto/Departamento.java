package com.examen.caso.dto;
import lombok.Data;

@Data
public class Departamento {
    private Integer codigo_departamento;
    private String nombre;
    private String ubicacion;
    private Departamento objDepartamento;
}
