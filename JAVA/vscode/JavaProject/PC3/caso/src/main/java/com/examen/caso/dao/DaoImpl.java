package com.examen.caso.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.examen.caso.dto.Actividad;
import com.examen.caso.dto.Departamento;
import com.examen.caso.dto.Empleado;
import com.examen.caso.dto.asignacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao{

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void crearConexion() throws SQLException{
            conexion = jdbcTemplate.getDataSource().getConnection();
    }
    private void cerrarConexion() throws SQLException{
        conexion.commit();
        conexion.close();
        conexion = null;
    }  
    public asignacion ObtenerAsignacion() {
        return null;
    }

    public List<Departamento> obtenerDepartamento() {
        return null;
    }

    public List<Empleado> obtenerEmpleado()  {
        List <Empleado> lista = new ArrayList <>();
        try {
            crearConexion();
            Statement sentencia = conexion.createStatement();
            String SQL = " SELECT e.nombres, e.apellidos, p.nombre " +
                    " FROM departamento p " +
                    " JOIN empleado e on (p.codigo_departamento = e.codigo_departamento)";
            ResultSet datos = sentencia.executeQuery(SQL);
            while(datos.next()){
                Empleado empleado = new Empleado();
                Departamento departamento = new Departamento();
                empleado.setNombres(datos.getString("nombres"));
                empleado.setApellidos(datos.getString("apellidos"));
                departamento.setNombre(datos.getString("nombre"));
                empleado.setobjDepartamento(departamento);
                lista.add(empleado);
            }
            datos.close();
            sentencia.close();
            cerrarConexion();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
    public Actividad nuevaActividad(Actividad nueva) {
        try {
            crearConexion();
            String SQL =" insert into actividad(id_actividad,nombre,prioridad)  "+
                        " VALUES ( ? , ? , ? ) ";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setInt(1, nueva.getId_actividad());
            sentencia.setString(2, nueva.getNombre());
            sentencia.setInt(3, nueva.getPrioridad());
            Integer actualizado = sentencia.executeUpdate();

            sentencia.close();
            cerrarConexion();      

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nueva;
    }
    
}
