package com.examen.caso.servicio;

import java.util.List;

import com.examen.caso.dao.Dao;
import com.examen.caso.dto.Actividad;
import com.examen.caso.dto.Empleado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio {
    @Autowired
    private Dao dao;
    public Actividad nuevaActividad(Actividad nueva) {
        return dao.nuevaActividad(nueva);
    }

    public List<Empleado> obtenerEmpleado() {
        return dao.obtenerEmpleado();
    }
    
}