package com.examen.caso.dao;

import java.util.List;

import com.examen.caso.dto.Actividad;
import com.examen.caso.dto.Departamento;
import com.examen.caso.dto.Empleado;
import com.examen.caso.dto.asignacion;

public interface Dao {
    
    public List <Empleado> obtenerEmpleado();
    public List <Departamento> obtenerDepartamento();
    public asignacion ObtenerAsignacion();
    public Actividad nuevaActividad(Actividad nueva);
}
