package com.examen.caso.servicio;

import java.util.List;

import com.examen.caso.dto.Actividad;
import com.examen.caso.dto.Empleado;

public interface Servicio {

    Actividad nuevaActividad(Actividad nueva);

    List<Empleado> obtenerEmpleado();
    
    
}

