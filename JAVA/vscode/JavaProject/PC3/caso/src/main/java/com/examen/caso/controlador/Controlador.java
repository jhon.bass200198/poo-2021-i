package com.examen.caso.controlador;

import com.examen.caso.dto.Actividad;
import com.examen.caso.dto.RespuestaEmpleado;
import com.examen.caso.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {
    @Autowired
    private Servicio servicio;
    
    
        @RequestMapping(value = "/nueva-actividad", method = RequestMethod.GET,
                        produces = "application/json;charset=utf-8",
                        consumes = "application/json;charset=utf-8")
        
    public @ResponseBody Actividad nuevaActividad(@RequestBody Actividad nueva){
        return servicio.nuevaActividad(nueva);
    }
        @RequestMapping(value = "/obtener-empleados", method = RequestMethod.GET,
                        produces = "application/json;charset=utf-8",
                        consumes = "application/json;charset=utf-8")

    public @ResponseBody RespuestaEmpleado obtenerEmpleado(){
            RespuestaEmpleado respuesta = new RespuestaEmpleado();
            respuesta.setLista(servicio.obtenerEmpleado());

            return respuesta;
    }
}

