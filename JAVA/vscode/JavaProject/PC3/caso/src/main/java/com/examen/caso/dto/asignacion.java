package com.examen.caso.dto;

import lombok.Data;

@Data
public class asignacion {
    private Integer id_asignación;
    private Integer codigo_empleado;
    private Integer id_actividad;
    private Integer presupuesto;
}
