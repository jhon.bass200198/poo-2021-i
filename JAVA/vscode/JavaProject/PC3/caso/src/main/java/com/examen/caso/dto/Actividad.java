package com.examen.caso.dto;

import lombok.Data;

@Data
public class Actividad {
    private Integer id_actividad;
    private String nombre;
    private Integer prioridad;
    
}
