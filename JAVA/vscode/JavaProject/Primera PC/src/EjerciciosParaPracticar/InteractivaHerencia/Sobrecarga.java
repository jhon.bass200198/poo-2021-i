package EjerciciosParaPracticar.InteractivaHerencia;

public class Sobrecarga {
    public static void main(String[] args){
        Persona alumno = new  Persona ("Jhonatan",23);
        Persona alumno2 = new  Persona (25);
        
        Persona.Trabajo obj = alumno.new Trabajo() ;
        Persona.Trabajo obj2 = alumno2.new Trabajo();
        

        System.out.println("Hola soy: "+ obj.Nombre("Jhonatan"));
        System.out.println("Tengo la edad de: " + obj.Edad(23));
        System.out.println("En 2 años tendre: " + obj2.Edad());
        
    }

}
