package EjerciciosParaPracticar.InteractivaHerencia;

import java.util.Scanner;

public class EjecutaExcepcion {
    public static void main(String[] args) {
        try {
               double x = Leer();
               System.out.println("Raiz cuadrada de " + x + " = " + Math.sqrt(x));                                   
        }catch (ValorNoValido opcion) {
                   System.out.println(opcion.getMessage());
        }
   }
   
   public static double Leer() throws ValorNoValido {
             Scanner objeto = new Scanner(System.in);
             System.out.print("Introduce número > 0 ");
             double n = objeto.nextDouble();
             if (n <= 0) {
                 throw new ValorNoValido("El número debe ser positivo");                                             
             }
             return n;
   }
}
