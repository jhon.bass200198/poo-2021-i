package EjerciciosParaPracticar.InteractivaHerencia;

import java.util.Scanner;

import EjerciciosParaPracticar.InteractivaHerencia.Tallas.Talla;

public class EjecutarTallas {
    
    public static void main(String [] args){
    Scanner llamada = new Scanner(System.in);

    System.out.println("Escoge tu talla: ");

    String llamada_datos = llamada.next().toUpperCase();
    Talla talla = Enum.valueOf(Talla.class,llamada_datos);
    
    System.out.println("Talla: " + talla);
    System.out.println("Representacion: "+ talla.Imprimir());
}
    
}
