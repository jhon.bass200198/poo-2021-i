package EjerciciosParaPracticar.InteractivaHerencia;

public class Tallas {
    enum Talla{
        STANDAR("S"),
        MEDIUM("M"),
        LARGE("L"),
        EXTRA_LARGE("XL");

        private Talla(String representacion){
            this.representacion = representacion;
        }
        public String Imprimir(){
            return representacion;
        }
        private String representacion;
    }
}
