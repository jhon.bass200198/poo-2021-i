package EjerciciosParaPracticar.InteractivaAbstracto;

public class Temp extends Objeto {
    
    Temp()
	{
		this(5);
		System.out.println("Constructor por defecto");
	}

	Temp(int x)
	{
		this(5, 15);
		System.out.println(x);
	}

	Temp(int x, int y)
	{
        super();
		System.out.println(x * y);
	}
    public static void main(String[] args)
	{
		Temp r = new Temp();
          
	}

}
