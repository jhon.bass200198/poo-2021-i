package EjerciciosParaPracticar.GetSet;

public class SegundaClase {
    
    public static void main(String[] args){
        PrimeraClase obj = new PrimeraClase();
        
        obj.setEdad(10);//set = estabecer
        System.out.println("La edad es: " + obj.getEdad());//get  =  mostrar
    }
}
