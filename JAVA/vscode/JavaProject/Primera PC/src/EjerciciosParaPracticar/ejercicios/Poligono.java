package EjerciciosParaPracticar.ejercicios;
import java.util.Set;
//Ejercicio2
public class Poligono {
    private int Lados;
    private double lado;
    private double ValorArea;
    private double ValorPerimetro;
    
    public double getArea() {
        return ValorArea;
    }
    public void setArea(double ValorArea) {
        this.ValorArea = ValorArea;
    }
    public double getLado() {
        return lado;
    }
    public void setLado(double lado) {
        this.lado = lado;
    }
    public int getLados() {
        return Lados;
    }
    public void setLados(int Lados) {
        this.Lados = Lados;
    }
    public double getPerimetro() {
        return ValorPerimetro;
    }
    public void setPerimetro(double ValorPerimetro) {
        this.ValorPerimetro = ValorPerimetro;
    }
    public Poligono (int Lados, double lado){
        this.Lados = Lados;
        this.lado = lado;
        Perimetro();
        Area();
    }
    public void Perimetro(){
        this.ValorPerimetro = this.Lados * this.lado;
    }
    public void Area(){
        double apotema = 0;
        apotema = this.lado / (2 * Math.tan((360/(2*this.Lados) * Math.PI /180)));
        this.ValorArea = this.ValorPerimetro * apotema /2 ;
    }
}
