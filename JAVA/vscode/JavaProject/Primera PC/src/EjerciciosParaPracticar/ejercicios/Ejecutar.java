package EjerciciosParaPracticar.ejercicios;
import java.util.Scanner;
//Ejercicio 2
public class Ejecutar{
    public static void main(String[] args) {
        int PrimerValor;
        double SegundoValor;

        Scanner input = new Scanner(System.in);

        System.out.println("Ingrese el numero de lados: ");
        PrimerValor = input.nextInt();
        
        if(PrimerValor < 7){
            System.out.println("Ingrese el valor del lado: ");
            SegundoValor = input.nextDouble();
            Poligono PolRegular = new Poligono(PrimerValor,SegundoValor);

            System.out.println("El Perimetro del poligono es: "+ PolRegular.getPerimetro());
            System.out.println("El Area del poligono es: "+ PolRegular.getArea());
            System.out.println("El valor de la diagonal es: " + Math.tan(45));
        }
        else{
            System.out.println("El numero de lados excede la capacidad de este programa.");
        }
    }
}


