package EjerciciosParaPracticar.ejercicios;
import java.util.Scanner;

public class Directorio {
    private String Nombre;
    private String Apellidos;
    private String Telefono;
    private String Correo;

    Directorio (){}

    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public String getApellidos(){
        return Apellidos;
    }
    public void setApellidos(String Apellidos){
        this.Apellidos = Apellidos;
    }
    public String getTelefono(){
        return Telefono;
    }
    public void setTelefono(String Telefono){
        this.Telefono = Telefono;
    }
    public String getCorreo(){
        return Correo;
    }
    public void setCorreo(String Correo){
        this.Correo = Correo;
    }

}
