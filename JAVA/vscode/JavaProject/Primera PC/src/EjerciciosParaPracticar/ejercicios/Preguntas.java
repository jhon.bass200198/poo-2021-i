package EjerciciosParaPracticar.ejercicios;
//Ejercicio 3
import java.util.Scanner;
public class Preguntas {
    public static void main(String[] args) {
        Pregunta[] preguntas = {
            new Pregunta("¿Como se llama el Profesor?", new Respuesta[] {
                new Respuesta("Rony Hancco", 'A', true),
                new Respuesta("Ronny Hancco", 'B', false),
                new Respuesta("Audante", 'C', false),
                new Respuesta("Cordova", 'D', false),
                new Respuesta("Acosta", 'E', false),
            }
            ),
            new Pregunta("¿Cuanto es 2x3?", new Respuesta[] {
                new Respuesta("12", 'A', false),
                new Respuesta("23", 'B', false),
                new Respuesta("6", 'C', true),
                new Respuesta("34", 'D', false),
                new Respuesta("4", 'E', false),
            }
            ),
            new Pregunta("¿Como se llama el curso?", new Respuesta[] {
                new Respuesta("Algoritmia y estructira de datos", 'A', false),
                new Respuesta("Programacion orientada a objetos", 'B', true),
                new Respuesta("Inroduccion a la computacion", 'C', false),
                new Respuesta("Base de Datos", 'D', false),
                new Respuesta("Java para novatos", 'E', false),
            }
            ),
            new Pregunta("¿Cuanto es 5 + 5?", new Respuesta[] {
                new Respuesta("1", 'A', false),
                new Respuesta("2", 'B', false),
                new Respuesta("3", 'C', false),
                new Respuesta("10", 'D', true),
                new Respuesta("20", 'E', false),
                
            })
        };
//Algunos pasos empleados fueron aprendidos por cuenta propia
        for (Pregunta Pregunton: preguntas) {
            Pregunton.preguntar();
        }
    }
}

class Pregunta {
    private String pregunta;
    private Respuesta[] Alternativas;
    public Pregunta(String pregunta, Respuesta[] Alternativas) {
        this.pregunta = pregunta;
        this.Alternativas = Alternativas;
    }

    public void preguntar() {
        System.out.println(this.pregunta);
        char ClaveCorrecta = 'A';
        for (Respuesta opcion: this.Alternativas) {
            if (opcion.esCorrecta()){
                ClaveCorrecta = opcion.getLetra();
            }
            System.out.print(String.valueOf(opcion.getLetra()) + ")" + opcion.getRespuesta() + " ");
        }
        System.out.println("\nElige tu respuesta: ");
        Scanner Test = new Scanner(System.in);
        char letraElegidaPorElUsuario = Test.next().toUpperCase().charAt(0);
        if (letraElegidaPorElUsuario == ClaveCorrecta){
            System.out.println("Correcto");
        }
        else{
            System.out.println("Incorrecto, la respuesta correcta era " + String.valueOf(ClaveCorrecta));
        }
    }
}
//Variables y llamado al resultado
class Respuesta {
    private String respuesta;
    private char letra;
    private boolean correcta;

    public Respuesta(String respuesta, char letra, boolean correcta) {
        this.respuesta = respuesta;
        this.letra = letra;
        this.correcta = correcta;
    }

    public String getRespuesta() {
        return this.respuesta;
    }

    public char getLetra() {
        return this.letra;
    }

    public boolean esCorrecta() {
        return this.correcta;
    }

}