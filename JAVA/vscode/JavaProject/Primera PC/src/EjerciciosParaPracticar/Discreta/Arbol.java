package EjerciciosParaPracticar.Discreta;

import java.util.Arrays;

public class Arbol {
    
    public void metodoPrim(Grafo grafo){
        int resultado = 0;

        int nodoVisitados[] = new int[grafo.cantidadNodos];
        nodoVisitados[0] = 1;

        int primerNodo = 0;
        int segundoNodo = 0;

        int valorMinimo = 10000;
        
        System.out.println("<> Arbol de expansion minima <>");

        for (int k = 1; k < grafo.cantidadNodos; k++){

            for (int i = 0; i < grafo.canidadNodos; i++){
                for( int j = 0; j < grafo.cantidadNodos; j++){
                    if(nodoVisitados[i] == 1 && nodoVisitados[j] == 0 && grafo.valores[i][j] < valorMinimo){
                        valorMinimo = grafo.valores[i][j];
                        primerNodo = i;
                        segundoNodo = j;
                    }
                }
            }
            resultado = resultado + valorMinimo;
            System.out.print("- Lado ("+grafo.nombreNodos[primerNodo]+",");
            System.out.print(grafo.nombreNodos[segundoNodo]+")";)
            System.out.println("--> Peso: "+valorMinimo);
            nodoVisitados[segundoNodo] = 1;
            valorMinimo = 10000;
        }
        System.out.println("\nResultado:" + resultado);
    }
}
