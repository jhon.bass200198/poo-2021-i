package EjerciciosParaPracticar.Discreta;

public class Grafo {
    int cantidadNodos;
    char[] nombreNodos;
    int[][] valores;

    public Grafo(int cantidadNodos){
        this.cantidadNodos = cantidadNodos;
        this.nombreNodos = new char[cantidadNodos];
        this.valores = new int[cantidadNodos][cantidadNodos];
    }
    public void agregarDatos(char nombreNodos[], int [][] valores){
        int i , j;
        for (i = 0; i < this.cantidadNodos; i++){
            this.nombreNodos [i] = nombreNodos [i];
            for (j = 0; j < this.cantidadNodos; j++){
                this.valores[i][j] = valores [i][j];
            }
        }
    }
}
