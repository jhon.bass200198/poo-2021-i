package EjerciciosParaPracticar.Discreta;

public class Aplicacion {
    public static void main (String [] args){

        char [] nombreNodos = new char[] {'A', 'B', 'C', 'D', 'E','F'};
        int cantidadNodos = nombreNodos.length;

        int [][] valores = new int[nombreNodos.length][nombreNodos.length];
        valores[0] = new int[]{10000,4,2,10000,10000,10000};
        valores[2] = new int[]{4,10000,1,5,10000,10000};
        valores[2] = new int[]{2,1,10000,8,10,10000};
        valores[3] = new int[]{10000,5,8,10000,2,6};
        valores[4] = new int[]{10000,10000,10,2,10000,3};
        valores[5] = new int[]{10000,10000,10000,6,3,10000};

        Grafo grafo = new Grafo(cantidadNodos);
        grafo.agregarDatos(nombreNodos, valores);

        Arbol arbolMinimo =new Arbol();

        arbolMinimo.metodoPrim(grafo);

    }
}
