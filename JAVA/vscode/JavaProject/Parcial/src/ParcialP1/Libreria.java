package ParcialP1;

public class Libreria{
    private String Nombre;
    private String autor;
    private String Editorial;
    private int fecha;
    private String codigo;
    
    
    Libreria (String Nombre,String autor, String Editorial, int fecha, String codigo){
        this.Nombre = Nombre;
        this.autor = autor;
        this.Editorial = Editorial;
        this.fecha = fecha;
        this.codigo = codigo;
    }
    
    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
        
    public String getNombre() {
        return Nombre;
    }


    public void setNombre(String nombre) {
        Nombre = nombre;
    }


    public String getEditorial() {
        return Editorial;
    }


    public void setEditorial(String editorial) {
        Editorial = editorial;
    }


    public int getFecha() {
        return fecha;
    }


    public void setFecha(int fecha) {
        this.fecha = fecha;
    }

    
}