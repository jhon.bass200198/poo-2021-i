package EjerciciosParaPracticar.InteractivaAbstracta;

public class Ejecutar {

    public static void main(String[] args){

        Cliente persona = new Cliente();
        Destino ubicacion = new Destino();
        persona.setNombre("Miguel");
        persona.setProducto("frutas");
        ubicacion.setMedio("maritimo");
        ubicacion.setDireccion("Brasil");
        persona.Comprar();
        System.out.println("El cliente "+persona.getNombre()+" Ordeno "+persona.getProducto());
        System.out.println("Y su destino es "+ubicacion.getDireccion()+" via transporte " + ubicacion.getMedio());
        
    }
    
}
