package EjerciciosParaPracticar.InteractivaAbstracta;

public abstract class Datos {
    private String nombre;
    private String producto;

    public abstract void Comprar();

    public Datos(){
        this.nombre = nombre;
        this.producto = producto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    } 
    
}
