package EjerciciosParaPracticar.InteractivaAbstracta;

public abstract class Transporte {
    private String medio;
    private String direccion;
    
    public Transporte(){
        this.medio = medio;
        this.direccion = direccion;
    }

    public String getMedio() {
        return medio;
    }

    public void setMedio(String medio) {
        this.medio = medio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    abstract public void Medio();
    
}
