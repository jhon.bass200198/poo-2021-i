public class OrdenarArreglos {
    
	public static void main(String[] args) {
    //Datos escritos previamnte, no llamados       
     	String[] arreglo={"Patricia","Jhonatan","Christian","Raul"};

     	int item, item2;
    
     	String auxiliar;
		//recorrido del arreglo y acomodo
     	for(item = 0; item < 3; item++){
			
        	for(item2 = item+1; item2 < 4; item2++){

              		if(arreglo[item].compareToIgnoreCase(arreglo[item2]) > 0){
 
                 		auxiliar = arreglo[item];
  
                 		arreglo[item] = arreglo[item2];
    
                 		arreglo[item2] = auxiliar;

              		}
  
           	 }

         }
		 //imprimiendo resultado
         for(item = 0; item < 4 ;item++){
 
         	if(item < 3){
  
             		System.out.print(arreglo[item]+",");
 
           	}
 
          	else{

               		System.out.print(arreglo[item]+".");

           	}

         }

    }

}
