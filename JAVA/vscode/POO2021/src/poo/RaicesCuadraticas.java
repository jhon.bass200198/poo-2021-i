package poo;

import java.util.Scanner;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.Math.abs;
public class RaicesCuadraticas {
    public static void main (String[] args){
       double a,b,c;
       System.out.println("ingrese los coeficientes: ");
       Scanner objeto = new Scanner(System.in);
       a = objeto.nextDouble();
       b = objeto.nextDouble();
       c = objeto.nextDouble(); 

       double discriminante  = pow(b,2) -4*a*c;
       if (discriminante >= 0){
           if (discriminante == 0){
           double x = -b/(2*a); //x es la raiz
           System.out.printf("Raiz unica es %.3f%n",x);
            }
            else{
            double x1 = (-b + sqrt(discriminante)/(2*a));
            double x2 = (-b - sqrt(discriminante)/(2*a));
            System.out.printf("Raiz x1 es %.3f%n",x1);
            System.out.printf("Raiz x2 es %.3f%n",x2);
            }
        }
        else{
            discriminante = abs(discriminante);
            double par_real = -b/(2*a);
            double par_imaginario = sqrt(discriminante/(2*a));
            System.out.printf("Raiz C x1 es %.3f + %.3fi%n",par_real,par_imaginario);
            System.out.printf("Raiz C x2 es %.3f- %.3fi%n",par_real,par_imaginario);
        }
    }
}
    

