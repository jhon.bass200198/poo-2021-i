package poo;

public class Pelota {
    float radio;
    float peso;
    String marca;
    public static void main (String[] args){
        Pelota MiPelota = new Pelota();
        MiPelota.radio = 10;
        MiPelota.peso =  100;
        MiPelota.marca = "Mikasa" ;

        System.out.println("El radio de la pelota es: "+  MiPelota.radio);
        System.out.println("El peso de la pelota es: "+  MiPelota.peso);
        System.out.println("La marca de la pelota es: "+  MiPelota.marca);
    }
}