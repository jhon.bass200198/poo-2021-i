package poo;

public class CifradoCesar {
    private String mayuscula;
    private String minuscula;

    public static void main(String[] args){
        //DEFINO EL ALFABETO CON STRING
        String mayuscula ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String minuscula = "abcdefghijklmnopqrstuvwxyz";
        CifradoCesar obj = new CifradoCesar (mayuscula, minuscula);
         
        //INVOCO EL RESULTADO //PALABRA A CIFRAR, se varia al probar el código
        System.out.println(obj.Convertir("ABCD",3));
        System.out.println(obj.descifrar("DEFG",3));
        
    }
    public CifradoCesar (String mayuscula, String minuscula){
        this.mayuscula = mayuscula;
        this.minuscula = minuscula;
    }
    //CONDICION PARA APLICAR CESAR INCLUIDO EL ESPACIO
    public String Convertir(String frase, int desplazar){
        String auxiliar = "";
        for (int item = 0; item < frase.length(); item++){
            if((this.mayuscula.indexOf(frase.charAt(item)) != -1)||(this.minuscula.indexOf(frase.charAt(item))!= -1)){
                auxiliar += (this.mayuscula.indexOf(frase.charAt(item)) != -1) ? this.mayuscula.charAt((this.mayuscula.indexOf(frase.charAt(item))+desplazar) % this.mayuscula.length()):this.minuscula.charAt((this.minuscula.indexOf(frase.charAt(item))+desplazar) % this.minuscula.length());
            }
            else{
                auxiliar += frase.charAt(item);
            }
        }//RESPUESTA DEL METODO REALIZADO
        return auxiliar;

    }//CODIGO PARA DESCIFRAR EL CESAR
    public String descifrar(String frase,int desplazar){
        String auxiliar = "";
        for (int item = 0; item < frase.length(); item++){
            if((this.mayuscula.indexOf(frase.charAt(item)) != -1)||(this.minuscula.indexOf(frase.charAt(item))!= -1)){
                if(this.mayuscula.indexOf(frase.charAt(item)) != -1){
                    if((this.mayuscula.indexOf(frase.charAt(item)) - desplazar) < 0){
                        auxiliar += this.mayuscula.charAt((this.mayuscula.length()) +((this.mayuscula.indexOf(frase.charAt(item))) - desplazar));
                    }
                    else{
                        auxiliar += this.mayuscula.charAt(((this.mayuscula.indexOf(frase.charAt(item))) - desplazar) % (this.mayuscula.length()));
                    }
                }
                else{
                    if(this.minuscula.indexOf(frase.charAt(item)) < 0){
                        auxiliar += this.minuscula.charAt((this.minuscula.length()) + ((this.minuscula.indexOf(frase.charAt(item)))- desplazar));
                    }
                    else{
                        auxiliar += this.minuscula.charAt(((this.minuscula.indexOf(frase.charAt(item))) - desplazar) % (this.minuscula.length()));
                    }
                }
            }
            else{
                auxiliar += frase.charAt(item);
            }
        }//RESPUESTA DEL METODO REALIZADO
        return auxiliar;
    }
}
