package poo;

import java.io.*;

public class AreaYPerimetroDeUnPoligonoRegular {

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        double apotema, area, longitud_de_lado, numero_de_lados, perimetro;
        System.out.print("Ingresa el valor de apotema: ");
        apotema = Double.parseDouble(in.readLine());
        System.out.print("Ingresa el valor de longitud de lado: ");
        longitud_de_lado = Double.parseDouble(in.readLine());
        System.out.print("Ingresa el valor de numero de lados: ");
        numero_de_lados = Double.parseDouble(in.readLine());
        perimetro=numero_de_lados*longitud_de_lado;
        area=apotema*apotema*numero_de_lados*Math.tan(Math.PI/numero_de_lados);
        System.out.println("Valor de area: " + area);
        System.out.println("Valor de perimetro: " + perimetro);
        in.close();
    }

}