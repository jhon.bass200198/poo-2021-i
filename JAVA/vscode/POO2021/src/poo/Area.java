package poo;

import java.util.Scanner;
public class Area {
    public static void main (String[] args){
        double radio, area;
        Scanner objeto = new Scanner(System.in);
        System.out.println("Valor del radio: ");
        radio = objeto.nextDouble();
        area = radio * radio * 3.1416;
        System.out.println("El área del circulo es: " + area);
    }
    
}