package poo;

import java.util.Scanner;
public class CifradoLibre {
    public static void main(String[] args){
        Scanner nuevo = new Scanner(System.in);
        String frase, cesar;
        int desplazar;
        cesar = "";

        String minuscula ="abcdefghijklmnopqrstuvwxyz";
        String mayuscula = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        System.out.print("Introduza la frase a desplazar: ");
        frase = nuevo.nextLine();

        System.out.print("Introduzca la clave de desplazamiento: ");
        desplazar = nuevo.nextInt();

        for (int item = 0; item < frase.length(); item ++){
            for (int item2 = 0; item2 < minuscula.length(); item2 ++){
                if (frase.charAt(item) == minuscula.charAt(item2)){
                    if (item2 + desplazar >= minuscula.length()){
                        cesar += minuscula.charAt((item2 + desplazar) % minuscula.length());
                    }
                    else{
                        cesar += minuscula.charAt(item2 + desplazar);
                    }
                }
                else if(frase.charAt(item) == mayuscula.charAt(item2)){
                    if(item2 + desplazar >= mayuscula.length()){
                        cesar += mayuscula.charAt((item2 + desplazar)%mayuscula.length());
                    }
                    else{
                        cesar += mayuscula.charAt(item2 + desplazar);
                    }
                }
            }
        }
        System.out.println(frase);
        System.out.println(cesar);
    }
}
    
