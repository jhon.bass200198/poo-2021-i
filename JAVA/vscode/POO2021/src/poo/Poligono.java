package poo;
import java.util.Scanner;

public class Poligono {
    double NoLados;
    double Lado;
    double Apotema;
    double Perimetro;
    double Area;
    
    Scanner L = new Scanner (System.in);
    
    public Poligono(){
        NoLados = 0;
        Lado = 0;
        Apotema = 0;
        Perimetro = 0;
        Area = 0;
    }
    
    public void leerNoLados(){
        System.out.println("Numeros de Laso del Poligono Regular:");
        NoLados = L.nextDouble();
    }
    
    public void leerLado(){
        System.out.println("Valor de los Lados:");
        Lado = L.nextDouble();
    }
    
    public void leerApotema(){
        System.out.println("Escribe valos del Apotema:");
        Apotema = L.nextDouble();
    }
    
    public void Perimetro(){
        Perimetro = NoLados * Lado;
    }
    
    
    public void Area(){
        Area = Perimetro * Apotema / 2;
    }
    
    public void Mostrar(){
        System.out.println("Numero de Lados del Poligono:" + NoLados);
        System.out.println("Valor de los Lados:" + Lado);
        System.out.println("El Perimetro del Poligono es:" + Perimetro);
        System.out.println("El Area del Poligono es:" + Area);
    }
    
}

