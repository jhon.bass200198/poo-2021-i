package poo;

public class Estatico {  
    public static void main(String args[]) {    
        Estudiante Alumno1 = new Estudiante();    
        Alumno1.showData();    
        Estudiante Alumno2 = new Estudiante();  
    }
} 
class Estudiante {
    int a; // variable iniciada en cero
    static int b; // inicia en cero solo cuando la clase no está cargada para cada objeto creado.   
 
    Estudiante() {  
        // Constructor para incrementar la variable b (estática)  
        b++; 
    }   
    public void showData() {     
            System.out.println("Valor de a es: " + a);     
            System.out.println("Valor de b es: " + b);  
        }
}