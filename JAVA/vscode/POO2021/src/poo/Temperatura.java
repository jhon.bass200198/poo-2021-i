package poo;

import java.util.Scanner;
public class Temperatura {
    public static void main (String[] args){
        Scanner lector = new Scanner(System.in);
        double K, C;

        System.out.println("Ingrese la temperatura en C°:");
        C = lector.nextDouble();

        K = C +273;
        System.out.println(C +" en gradios Kelvin es: "+ K +" K");
    
    } 
}
