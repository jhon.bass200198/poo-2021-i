package poo;

class Temp{
	
	Temp()
	{
		this(5);
		System.out.println("Constructor por defecto");
	}

	Temp(int x){
		this(5, 15);
		System.out.println(x);
	}

	Temp(int x, int y){
		System.out.println(x * y);
	}
	public static void main(String args[])
	{
		new Temp();
        //r.Temp(2,3); no puedes llamar un constructor de esta forma
	}
}
