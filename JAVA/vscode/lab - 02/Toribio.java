package poo;

import java.util.Scanner;
public class Toribio {
    public static int invertir(int numero){
        int invertir = 0,residuo;
        while(numero>0){
            residuo = numero % 10;
            invertir = (invertir*10) + residuo;
            numero /= 10;
        }
        return invertir;
    }
    public static int CambioBase9(int suma){
        int cont = 0,resultado = 0,cociente = 0,resta = 0,basico = 9,modificando = 0,potencia;
        modificando=suma;
        while(modificando>0){
            resta = modificando % basico;
            cociente =  modificando/basico;
            modificando = cociente;
            potencia = (int)  Math.pow(10,cont);
            resultado = (resta * potencia) + resultado;
            cont = cont + 1;
        }
        potencia=(int)  Math.pow(10,cont);
        resultado = cociente* potencia + resultado;
        return resultado;
    }
    public static boolean  ValidacionBase(int numero, int base){
        int prueba=0;
        while(numero > 0) {
            prueba= numero % 10;
            numero = numero / 10;
            if(prueba >= base){
                return false;
            }
        }
        return true;
    }
    public static int CambioBase10(int numero, int base, int longitud){
        int ResultadoFinal = 0,resto,potencia;
        for(int item = longitud - 1; item >= 0; item --){
            resto = numero % 10;
            potencia = (int)  Math.pow(base,item);
            ResultadoFinal = ResultadoFinal+ resto*potencia;
            numero /= 10;
        }
        return ResultadoFinal;
    }
    public static int LongNumero(int numero){
        int cant_cifras = 0;
        while (numero != 0){
            numero /= 10;        
            cant_cifras ++; 
        }            
        return cant_cifras;         
    }
    public static void main(String[] args){
        Scanner indice = new Scanner (System.in);
        int numero1, numero2, base1, base2;
        int cambio = 0, segundo_cambio = 0, inversion = 0, segunda_inversion = 0;
        int primer_long,segunda_long;
        int suma = 0;
        int resultado;

        System.out.println("Primer numero: ");
        numero1 = indice.nextInt();
        System.out.println("Base del primer numero: ");
        base1 = indice.nextInt();
        System.out.println("Segundo numero: ");
        numero2 = indice.nextInt();
        System.out.println("Base del segundo numero: ");
        base2 = indice.nextInt();

        if((ValidacionBase(numero1,base1) == true) && (ValidacionBase(numero2,base2) == true)){
            inversion = invertir(numero1);
            segunda_inversion = invertir(numero2);
            primer_long = LongNumero(numero1);
            segunda_long = LongNumero(numero2);
            cambio = CambioBase10(inversion,base1,primer_long);
            segundo_cambio = CambioBase10(segunda_inversion,base2,segunda_long);
            suma = cambio + segundo_cambio;
            resultado = CambioBase9(suma);
            System.out.println("Suma en base 9 es: " + resultado);
            }
        else{
            System.out.println("No hay logica en la operacion.");
        }
    } 
}