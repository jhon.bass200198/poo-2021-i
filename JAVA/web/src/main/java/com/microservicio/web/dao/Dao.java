package com.microservicio.web.dao;

import java.util.List;

import com.microservicio.web.dto.Marca;
import com.microservicio.web.dto.Producto;


public interface Dao {
    public List<Producto> obtenerProducto(Producto producto);
}
