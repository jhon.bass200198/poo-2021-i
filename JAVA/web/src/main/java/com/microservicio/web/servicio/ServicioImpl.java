package com.microservicio.web.servicio;

import java.util.List;

import com.microservicio.web.dao.Dao;
import com.microservicio.web.dto.Marca;
import com.microservicio.web.dto.Producto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public List<Producto> obtenerProducto(Producto producto) {
        return dao.obtenerProducto(producto);
    }
}
