package com.microservicio.web.servicio;

import com.microservicio.web.dto.Producto;

import java.util.List;

public interface Servicio {
    public List<Producto> obtenerProducto(Producto producto);
}
    
