package com.example.demo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.dto.Alumno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class DaoImpl implements Dao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection connection;

    private void crearConnection() {
        try {
            connection = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConnection(ResultSet resultSet, Statement statement) {
        try {
            if(resultSet != null) resultSet.close();
            if(statement != null) statement.close();
            connection.commit();
            connection.close();
            connection = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<Alumno> obtenerAlumnos() {
        List<Alumno> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("select codigo, nombre, apellido ").
                append(" from alumno");

        crearConnection();
        try {

            Statement sentencia = connection.createStatement();
            ResultSet resultado = sentencia.executeQuery(sb.toString());
            while(resultado.next()) {
                Alumno alumno = new Alumno();
                alumno.setCodigo(resultado.getInt("codigo"));
                alumno.setNombre(resultado.getString("nombre"));
                alumno.setApellido(resultado.getString("apellido"));
                lista.add(alumno);
            }
            cerrarConnection(resultado, sentencia);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }

    @Override
    public Alumno obtenerAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public Alumno agregarAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public Alumno actualizarAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public List<Alumno> buscarAlumnos(Alumno alumno) {
        return null;
    }
}