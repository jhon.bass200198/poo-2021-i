package com.example.demo.dto;
import lombok.Data;

@Data
public class Notas {
    private Alumno codigo;
    private Integer Nota1;
    private Integer Nota2;
    private Integer Nota3;
} 