package com.example.demo.servicio;

import java.util.List;

import com.example.demo.dao.Dao;
import com.example.demo.dto.Alumno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio {

    @Autowired
    private Dao dao;

    @Override
    public List<Alumno> obtenerAlumnos() {
        return dao.obtenerAlumnos();
    }

    @Override
    public Alumno obtenerAlumno(Alumno alumno) {
        return dao.obtenerAlumno(alumno);
    }

    @Override
    public Alumno agregarAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public Alumno actualizarAlumno(Alumno alumno) {
        return null;
    }

    @Override
    public List<Alumno> buscarAlumnos(Alumno alumno) {
        return null;
    }
}
