package com.example.demo.controlador;

import com.example.demo.dto.RespuestaAlumno;
import com.example.demo.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    
    @Autowired
    private Servicio servicio;
    
    @RequestMapping(value = "/obtener-alumnos",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody RespuestaAlumno obtenerAlumnos() {
        RespuestaAlumno respuestaAlumno = new RespuestaAlumno();
        respuestaAlumno.setLista(servicio.obtenerAlumnos());
        return respuestaAlumno;
    }
}