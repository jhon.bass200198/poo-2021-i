package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.Alumno;


public interface Dao {
    public List<Alumno> obtenerAlumnos();
    public Alumno obtenerAlumno(Alumno alumno);

    public Alumno agregarAlumno(Alumno alumno);

    public Alumno actualizarAlumno(Alumno alumno);

    public List<Alumno> buscarAlumnos(Alumno alumno);
}
