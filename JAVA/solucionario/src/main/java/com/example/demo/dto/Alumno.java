package com.example.demo.dto;
import lombok.Data;

@Data
public class Alumno {
    private Integer codigo;
    private String nombre;
    private String apellido;    
}

