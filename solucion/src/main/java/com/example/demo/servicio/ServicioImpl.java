package com.example.demo.servicio;


import java.util.List;

import com.example.demo.dao.Dao;
import com.example.demo.dto.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;


    @Override
    public void CambiarApellido(Usuario usuario, String apellido) {
        dao.CambiarApellido(usuario,apellido);
    }

    @Override
    public void Cambiarclave(Usuario usuario, String clave) {
        dao.Cambiarclave(usuario,clave);
    }

    @Override
    public void CambiarCorreo(Usuario usuario, String correo) {
        dao.CambiarCorreo(usuario,correo);
    }

    @Override
    public void CambiarNombre(Usuario usuario, String nombre) {
        dao.CambiarNombre(usuario,nombre);
    }

    @Override
    public List<Usuario> ObtenerUsuarios(Usuario usuario) {
        return dao.ObtenerUsuarios(usuario);
    }
}

