package com.example.demo.servicio;
import java.util.List;

import com.example.demo.dto.Usuario;

public interface Servicio {
    //Actualizacion
    public void CambiarNombre(Usuario usuario, String nombre);
    public void CambiarApellido(Usuario usuario, String apellido);
    public void CambiarCorreo(Usuario usuario, String correo);
    public void Cambiarclave(Usuario usuario, String clave);

    //Obtencion
    public List<Usuario> ObtenerUsuarios(Usuario usuario);
}

