package com.example.demo.dto.GuiaEnunciado;

import com.example.demo.dto.Usuario;

import lombok.Data;

@Data
public class CambioNombre {
    private Usuario usuario;
    private String nombre;
}
