package com.example.demo.dto;

import lombok.Data;

@Data
public class Usuario {
    private Integer id_usuario;
    private String nombres;
    private String apellidos;
    private String correo;
    private String administrador;
    private String clave;
}

