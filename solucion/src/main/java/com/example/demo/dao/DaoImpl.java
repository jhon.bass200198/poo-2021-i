package com.example.demo.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.dto.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Actualizacion de usuario
    @Override
    public void CambiarNombre(Usuario usuario, String nombre) {
        String sql = " update usuario set nombres=? where id_usuario=?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombre);
            sentencia.setInt(2, usuario.getId_usuario());
            Integer variable = sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    @Override
    public void CambiarApellido(Usuario usuario, String apellido) {
        String sql = " update usuario set apellidos=? where id_usuario=?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, apellido);
            sentencia.setInt(2, usuario.getId_usuario());
            Integer variable = sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Cambiarclave(Usuario usuario, String clave) {
        String sql = " update usuario set clave=? where id_usuario=?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, clave);
            sentencia.setInt(2, usuario.getId_usuario());
            Integer variable = sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void CambiarCorreo(Usuario usuario, String correo) {
        String sql = " update usuario set correo=? where id_usuario=?";
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, correo);
            sentencia.setInt(2, usuario.getId_usuario());
            Integer variable = sentencia.executeUpdate();
            cerrarConexion(null,sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Obtencion de los usuarios administradores
    @Override
    public List<Usuario> ObtenerUsuarios(Usuario usuario) {
        List<Usuario> usuarios = new ArrayList<>();
        String sql = "select id_usuario,nombres,apellidos,correo,administrador,clave from usuario\n" +
                "where upper(nombre) like ? or upper(apellidos) like ? or upper(correo) like ? and administrador=2";
        Usuario usuarioAux = new Usuario();
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, "%"+usuario.getNombres().toUpperCase()+"%");
            sentencia.setString(2, "%"+usuario.getApellidos().toUpperCase()+"%");
            sentencia.setString(3, "%"+usuario.getCorreo().toUpperCase()+"%");
            ResultSet obtenido = sentencia.executeQuery();

            while (obtenido.next()) {
                usuarioAux.setId_usuario(obtenido.getInt("id_usuario"));
                usuarioAux.setNombres(obtenido.getString("nombres"));
                usuarioAux.setApellidos(obtenido.getString("apellidos"));
                usuarioAux.setCorreo(obtenido.getString("correo"));
                usuarioAux.setAdministrador(obtenido.getString("administrador"));
                usuarioAux.setClave(obtenido.getString("clave"));              
                usuarios.add(usuarioAux);
            }
            cerrarConexion(obtenido,sentencia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuarios;
    }
}