package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.Usuario;

public interface Dao {

    public void CambiarApellido(Usuario usuario, String apellido);
    public void Cambiarclave(Usuario usuario, String clave);
    public void CambiarCorreo(Usuario usuario, String correo);
    public void CambiarNombre(Usuario usuario, String nombre);
    public List<Usuario> ObtenerUsuarios(Usuario usuario);
}
