package com.example.demo.controlador;

import com.example.demo.dto.RespuestaUsuario;
import com.example.demo.dto.Usuario;
import com.example.demo.dto.GuiaEnunciado.CambiarCorreo;
import com.example.demo.dto.GuiaEnunciado.CambioApellido;
import com.example.demo.dto.GuiaEnunciado.CambioClave;
import com.example.demo.dto.GuiaEnunciado.CambioNombre;
import com.example.demo.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    //Actualizacion
    @RequestMapping(value = "/cambiar-nombre", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
    public void cambiarNombre(@RequestBody CambioNombre cambioNombre) {
        servicio.CambiarNombre(cambioNombre.getUsuario(), cambioNombre.getNombre());
    }

    @RequestMapping(value = "/cambiar-apellido", method = RequestMethod.POST,
    produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
    public void cambiarApellido(@RequestBody CambioApellido cambioApellido) {
        servicio.CambiarApellido(cambioApellido.getUsuario(), cambioApellido.getApellido());
    }
    
    @RequestMapping(value = "/cambiar-correo", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
    public void cambiarCorreo(@RequestBody CambiarCorreo cambioCorreo) {
        servicio.CambiarCorreo(cambioCorreo.getUsuario(), cambioCorreo.getCorreo());
    }

    @RequestMapping(value = "/cambiar-clave", method = RequestMethod.POST,
            produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
    public void cambiarClave(@RequestBody CambioClave cambioClave) {
        servicio.Cambiarclave(cambioClave.getUsuario(), cambioClave.getClave());
    }
//Obtencion de usuarios
    @RequestMapping(method = RequestMethod.POST, value = "/obtener-usuarios",
            produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")

    public @ResponseBody RespuestaUsuario obtenerUsuarios(@RequestBody Usuario usuario) {
        RespuestaUsuario usuarios = new RespuestaUsuario();
        usuarios.setUsuarios(servicio.ObtenerUsuarios(usuario));
        return usuarios;
    }
}